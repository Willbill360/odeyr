#!/usr/bin/env node
const program = require('commander')
const { prompt } = require('inquirer')
const axios = require('axios')

program
  .version('1.0.0')
  .description('An interactive CLI Tool to interact with the Odeyr API')

program
  .command('setup')
  .alias('register')
  .description('Initialise the setup/registering sequence')
  .action(() => {
    prompt([
      {
        type: 'input',
        name: 'name',
        message: 'Discord guild/server name:'
      },
      {
        type: 'input',
        name: 'channel',
        message: 'Channel name (where the IP will be posted):'
      },
      {
        type: 'input',
        name: 'password',
        message: 'Password (to recover your token):'
      }
    ]).then(answers => {
      console.log(answers)
    })
  })

program.parse(process.argv)

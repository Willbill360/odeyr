const fs = require('fs')
const Discord = require('discord.js')
const client = new Discord.Client()
let bot = {}

function parseTemplatedMessage (serverIP) {
  let message = fs.readFileSync('./services/messageTemplate.md', 'utf8')
  message = message.replace(/{server_ip}/g, serverIP)
  return message
}

bot.sendMessage = async function (guildName, channelName, serverIP) {
  let guild = this.client.guilds.find(guild => guild.name === guildName)
  let channel = guild.channels.find(channel => channel.name === channelName)

  channel.fetchMessages({ limit: 100 }).then(async (messages) => {
    messages = messages.filter(message => message.author.bot)
    messages.forEach(msg => {
      msg.delete()
    })

    let message = await channel.send(parseTemplatedMessage(serverIP))
    message.pin()
  })
}

bot.getInvite = async function () {
  let link = await this.client.generateInvite([
    'SEND_MESSAGES',
    'MANAGE_MESSAGES',
    'VIEW_CHANNEL',
    'MANAGE_CHANNELS',
    'MANAGE_MESSAGES',
    'EMBED_LINKS',
    'READ_MESSAGE_HISTORY',
    'MENTION_EVERYONE'
  ])

  return link
}

bot.client = client

module.exports = bot

const passport = require('passport')
const { Guild } = require('../models')

const HeaderAPIKeyStrategy = require('passport-headerapikey').HeaderAPIKeyStrategy

passport.use(new HeaderAPIKeyStrategy(
  { header: 'Authorization', prefix: 'Api-Key ' },
  false,
  async function (apikey, done) {
    Guild.findOne({ where: { token: apikey } }).then((guild, err) => {
      if (err) { return done(err) }
      if (!guild) { return done(null, false) }
      return done(null, guild)
    })
  }
))

module.exports = null

const chalk = require('chalk')
const express = require('express')
const fs = require('fs-extra')
const http = require('http')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const config = require('./config/config')

const v1 = require('./routes/v1')
const app = express()

// Create the http server
let server = http.createServer(app)

// App initialisation
if (config.enableMorgan) {
  app.use(morgan('dev'))
}
app.use(bodyParser.json())
app.use(cors())

require('./services/passport')

// Version Routes
app.use('/v1', v1)

// Catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.sendStatus(404)
})

// DATABASE, DISCORD BOT and SERVER start
const models = require('./models')
const discord = require('./services/discordBot')
models.sequelize.authenticate().then(() => {
  console.log(chalk`\n{bold Connected to SQL database:} {cyan ${config.db.database}}`)
  discord.client.login(process.env.DISCORD_TOKEN)
}).catch(err => {
  console.error(chalk`{red Unable to connect to SQL database:} {cyan ${config.db.database}}`, err)
})
if (process.env.NODE_ENV === 'development') {
  models.sequelize.sync() // creates table if they do not already exist
  // models.sequelize.sync({ force: true }) // deletes all tables then recreates them useful for testing and development purposes
}

discord.client.on('ready', () => {
  console.log(chalk`{bold Discord Bot:} {green READY}`)
  server.listen(config.port, () => {
    console.log(chalk`{bold Serveur Running on port:} {cyan ${config.port}}`)
    console.log(chalk`{bold STATE:} {green ${process.env.NODE_ENV || 'developpement'}} | {bold HTTPS:} {green ${config.https}} | {bold MORGAN:} {green ${config.enableMorgan}}`)
  })
  server.on('error', onError)
})

/**
 * Event listener for HTTP server "error" event.
 */
function onError (error) {
  if (error.syscall !== 'listen') {
    throw error
  }

  const bind = typeof config.port === 'string'
    ? 'Pipe ' + config.port
    : 'Port ' + config.port

  // Handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges')
      process.exit(1)
    case 'EADDRINUSE':
      console.error(bind + ' is already in use')
      process.exit(1)
    default:
      throw error
  }
}

/**
 * Event listener for uncaught exception event
 */
process.on('unhandledRejection', console.log.bind(console))
process.on('uncaughtException', function (err) {
  let d = new Date()
  let datestring = d.getFullYear() + '-' + ('0' + (d.getMonth() + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2) + ':' + ('0' + d.getSeconds()).slice(-2)

  console.log(chalk`{bold.red (${datestring}) Caught exception: \n} ${err}`)
  fs.outputFile(`/logs/${datestring}.log`, err)
})

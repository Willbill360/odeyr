const passport = require('passport')
const { ReE } = require('../services/utils')

module.exports = function (req, res, next) {
  passport.authenticate('headerapikey', { session: false }, function (err, guild) {
    if (err || !guild) {
      return ReE(res, 'You must be authenticated to access this ressource', 401)
    } else {
      req.guild = guild
      next()
    }
  })(req, res, next)
}

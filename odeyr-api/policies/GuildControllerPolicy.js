const Joi = require('joi')
const { ReE } = require('../services/utils')

module.exports = {
  register (req, res, next) {
    const schema = {
      name: Joi.string().required(),
      password: Joi.string().min(8).required(),
      channel: Joi.string().required()
    }

    const { error } = Joi.validate(req.body, schema)
    if (error) return ReE(res, error, 400)

    next()
  },
  authenticate (req, res, next) {
    const schema = {
      name: Joi.string().required(),
      password: Joi.string().required()
    }

    const { error } = Joi.validate(req.body, schema)
    if (error) return ReE(res, error, 400)

    next()
  },
  update (req, res, next) {
    const schema = {
      name: Joi.string(),
      password: Joi.string().min(8),
      channel: Joi.string()
    }

    const { error } = Joi.validate(req.body, schema)
    if (error) return ReE(res, error, 400)

    next()
  },
  newIp (req, res, next) {
    const schema = {
      serverIP: Joi.string()
    }

    const { error } = Joi.validate(req.body, schema)
    if (error) return ReE(res, error, 400)

    next()
  }
}

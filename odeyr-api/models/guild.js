'use strict'
const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcrypt-nodejs'))

module.exports = (sequelize, DataTypes) => {
  const Guild = sequelize.define('Guild', {
    name: {
      allowNull: false,
      unique: true,
      type: DataTypes.STRING
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING
    },
    channel: {
      allowNull: false,
      type: DataTypes.STRING
    },
    token: {
      allowNull: false,
      type: DataTypes.STRING
    }
  }, {})
  Guild.associate = function (models) {
    // associations can be defined here
  }

  Guild.beforeSave(async (guild, options) => {
    if (!guild.changed('password')) {
      return
    }

    return bcrypt
      .genSaltAsync(10)
      .then(salt => bcrypt.hashAsync(guild.password, salt, null))
      .then(hash => {
        guild.setDataValue('password', hash)
      })
  })

  Guild.prototype.comparePassword = function (password) {
    return bcrypt.compareAsync(password, this.password)
  }

  Guild.prototype.toJSON = function () {
    let values = this.get()

    delete values.password
    return values
  }

  return Guild
}

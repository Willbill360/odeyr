const fs = require('fs')
const path = require('path')
const Sequalize = require('sequelize')
const config = require('../config/config')
const db = {}

const sequelize = new Sequalize(
  config.db
)

fs
  .readdirSync(__dirname)
  .filter((file) => file !== 'index.js')
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file))
    db[model.name] = model
  })

Object.keys(db).forEach(function (modelName) {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequalize = Sequalize

module.exports = db

require('dotenv').config()

let options = {
  dialect: 'sqlite',
  storage: './storage/database.sqlite'
}

module.exports = {
  development: options,
  test: options,
  production: options
}

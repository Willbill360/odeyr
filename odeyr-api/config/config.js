require('dotenv').config()

module.exports = {
  port: process.env.PORT || 8081,
  https: JSON.parse(process.env.HTTPS_ENABLED),
  enableMorgan: JSON.parse(process.env.MORGAN_ENABLED),
  db: {
    dialect: 'sqlite',
    storage: './storage/database.sqlite',
    operatorsAliases: require('sequelize').Op,
    pool: {
      max: 10,
      min: 0,
      idle: 10000
    }
  },
  apiKeys: {},
  authentication: {
    jwtSecret: process.env.JWT_SECRET || 'secret'
  }
}

module.exports = {
  apps: [{
    name: 'Odeyr API',
    script: 'app.js',
    args: [
      '--color'
    ],
    instances: 1,
    autorestart: true,
    watch: false,
    'ignore_watch': ['storage'],
    max_memory_restart: '1G'
  }],
  deploy: {}
}

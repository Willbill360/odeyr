/* eslint-disable no-unused-vars */
/* eslint-disable no-unexpected-multiline */
/* eslint-disable no-sequences */
const { Guild } = require('../models')
const { to, ReE, ReS } = require('../services/utils')
const discordBot = require('../services/discordBot')
const tokenGenerator = require('crypto-token')

module.exports.register = async function (req, res) {
  req.body.token = tokenGenerator(32)

  Guild.create(req.body).then(async (guild) => {
    discordBot.getInvite().then(link => {
      return ReS(res, { token: req.body.token, link })
    })
  }).catch((err) => {
    if (err.message === 'Validation error') {
      return ReE(res, 'A guild with that name already exist')
    }

    return ReE(res, err, 400)
  })
}

// TODO: Check if a try catch is nessesary
module.exports.authenticate = async function (req, res) {
  const guild = await Guild.findOne({ where: { name: req.body.name } })
  if (!guild) return ReE(res, 'There are no guild/server with that name', 401)

  const isPasswordValid = await guild.comparePassword(req.body.password)
  if (!isPasswordValid) return ReE(res, 'Invalid password', 401)

  return ReS(res, { token: guild.toJSON().token })
}

module.exports.update = async function (req, res) {
  let guild = req.guild

  try {
    guild = await guild.update(req.body)
  } catch (err) {
    return ReE(res, err, 400)
  }

  return ReS(res, { guild: guild.toJSON() })
}

module.exports.newIp = async function (req, res) {
  let guild = req.guild

  discordBot.sendMessage(guild.name, guild.channel, req.body.serverIP).then(() => {
    return ReS(res, { message: `Discord message sent and pined on ${guild.name} #${guild.channel}` })
  })
}

module.exports.getInvite = async function (req, res) {
  discordBot.getInvite().then(link => {
    return ReS(res, { link })
  })
}

const express = require('express')
const router = express.Router()

//  Controllers
const GuildController = require('../controllers/GuildController')

//  Policies
const isAuthenticated = require('../policies/isAuthenticated')
const GuildControllerPolicy = require('../policies/GuildControllerPolicy')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.json({ status: 'success', message: 'Odeyr API', data: { 'version_number': 'v1.0.0' } })
})

// Guilds (of discord)
router.post('/guilds/register', GuildControllerPolicy.register, GuildController.register)
router.post('/guilds/authenticate', GuildControllerPolicy.authenticate, GuildController.authenticate)
router.put('/guilds/new-ip', isAuthenticated, GuildControllerPolicy.newIp, GuildController.newIp)
router.get('/guilds/get-invite', isAuthenticated, GuildController.getInvite)
router.put('/guilds/update', isAuthenticated, GuildControllerPolicy.update, GuildController.update)

module.exports = router

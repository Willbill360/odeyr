# Odeyr

Odeyr is a Node.js API, Discord Bot and CLI Tool used to manage minecraft servers. 
Odeyr has been made in the attempt to reduce human labor when it comes to hosting a minecraft server on your own and providing a public IP via ngrok. 

The main functionnalities are:
* [ ] Launch the minecraft server
* [ ] Launch the ngrok server
* [X] Publish on discord via the bot the new IP address

Things that are still on the todo list:
* [ ] A web interface to see all the infos nessarry + a list of current connected users, quick action (restart, send command, etc.)
* [ ] The ability to restart the server by messaging the discord bot
* [ ] A more secure way of creating account, at this moment anyone can register a the discord bot for any discord guild. (Maybe only let the owner of the discord being able to setup the bot)